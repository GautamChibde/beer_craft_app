package beercraft.chibde.com.beercraft.main

import beercraft.chibde.com.beercraft.model.BeerItem
import beercraft.chibde.com.beercraft.model.CartItem

/**
 * Created by gautam on 30/6/18.
 */
interface MainActivityView {
    fun onLoadBeerItems(items : List<BeerItem>)
    fun showProgressDialog()
    fun hideProgressDialog()
    fun addItemToCart(beerItem: BeerItem)
    fun getCartItems() : List<CartItem>
    fun getBeerItems() : List<BeerItem>
    fun setItemTotal(size: Int)
    fun hideBottomView()
    fun showBottomView()
    fun filteredItems(filter: List<BeerItem>)
    fun onError(msg: String)
    fun setSortedItems(items: List<BeerItem>)
    fun sortedIconDesc()
    fun sortedIconAsce()

}