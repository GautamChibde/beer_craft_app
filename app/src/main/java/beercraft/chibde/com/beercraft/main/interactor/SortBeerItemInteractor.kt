package beercraft.chibde.com.beercraft.main.interactor

import beercraft.chibde.com.beercraft.main.MainActivityPresenter
import beercraft.chibde.com.beercraft.model.BeerItem
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


/**
 * Created by gautam on 30/6/18.
 */
class SortBeerItemInteractor() {
    fun execute(mode : Int, items: List<BeerItem>) : Single<List<BeerItem>> {
        return Observable.fromIterable(items)
                .toSortedList { o1, o2 ->
                    when {
                        o1.getAlcoholContent() == o2.getAlcoholContent() -> return@toSortedList 0
                        o1.getAlcoholContent() > o2.getAlcoholContent() -> return@toSortedList if (mode == MainActivityPresenter.SORTED_DESC) 1 else -1
                        o1.getAlcoholContent() < o2.getAlcoholContent() -> return@toSortedList if (mode == MainActivityPresenter.SORTED_DESC) -1 else 1
                        else -> return@toSortedList 0
                    }
                }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}