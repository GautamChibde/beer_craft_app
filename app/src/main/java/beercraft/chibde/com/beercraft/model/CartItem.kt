package beercraft.chibde.com.beercraft.model

/**
 * Created by gautam on 30/6/18.
 */
data class CartItem(var beerItem: BeerItem,
                    var quantity: Int) {
}