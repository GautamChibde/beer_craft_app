package beercraft.chibde.com.beercraft

import android.app.Application

/**
 * Created by gautam on 30/6/18.
 */
class BeerCraftApp : Application() {
    override fun onCreate() {
        super.onCreate()
        ApiClient.init()
    }
}