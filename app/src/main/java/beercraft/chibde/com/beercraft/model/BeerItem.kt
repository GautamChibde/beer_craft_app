package beercraft.chibde.com.beercraft.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by gautam on 30/6/18.
 */
data class BeerItem(var id: Long,
                    @SerializedName("abv") var alcoholByVolume: String = "0",
                    @SerializedName("ibu") var bitternessUnit: String,
                    var name: String,
                    @SerializedName("ounces") var weight: Double,
                    var style: String) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readLong(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readDouble(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(alcoholByVolume)
        parcel.writeString(bitternessUnit)
        parcel.writeString(name)
        parcel.writeDouble(weight)
        parcel.writeString(style)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BeerItem> {
        override fun createFromParcel(parcel: Parcel): BeerItem {
            return BeerItem(parcel)
        }

        override fun newArray(size: Int): Array<BeerItem?> {
            return arrayOfNulls(size)
        }
    }

    fun getAlcoholContent(): Double {
        return if(alcoholByVolume.isEmpty()) {
            0.0
        } else {
            alcoholByVolume.toDouble()
        }
    }
}

