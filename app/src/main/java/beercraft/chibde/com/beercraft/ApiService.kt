package beercraft.chibde.com.beercraft

import beercraft.chibde.com.beercraft.model.BeerItem
import io.reactivex.Single
import retrofit2.http.GET

/**
 * Created by gautam on 30/6/18.
 */
interface ApiService {
    @GET("beercraft")
    fun getBeerItems() : Single<List<BeerItem>>
}