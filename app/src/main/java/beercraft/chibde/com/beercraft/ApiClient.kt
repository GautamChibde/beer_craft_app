package beercraft.chibde.com.beercraft

import android.content.Context
import android.text.TextUtils
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.Retrofit



/**
 * Created by gautam on 30/6/18.
 */
class ApiClient {

    companion object {
        var retrofit: Retrofit? = null

        fun getInstance(): Retrofit? {
            return retrofit!!
        }

        fun init() {
            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                        .baseUrl(Constants.BASE_URL)
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build()
            }
        }
    }
}