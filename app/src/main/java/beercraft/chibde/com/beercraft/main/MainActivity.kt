package beercraft.chibde.com.beercraft.main

import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.appcompat.widget.SearchView
import beercraft.chibde.com.beercraft.R
import beercraft.chibde.com.beercraft.main.adapter.BeerItemAdapter
import beercraft.chibde.com.beercraft.main.adapter.CartItemAdapter
import beercraft.chibde.com.beercraft.model.BeerItem
import beercraft.chibde.com.beercraft.model.CartItem
import com.afollestad.materialdialogs.MaterialDialog
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar_main_activity.*
import org.jetbrains.anko.alert

class MainActivity : AppCompatActivity(), MainActivityView, SearchView.OnQueryTextListener {
    private val presenter: MainActivityPresenter by lazy { MainActivityPresenter(this) }
    private lateinit var dialog: MaterialDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initCartView()
        initViews()
        savedInstanceState?.let {
            onLoadBeerItems(savedInstanceState.getParcelableArrayList(BEER_ITEMS))
        } ?: run {
            presenter.onInit()
        }
    }

    private fun initViews() {
        toolbar_main_activity_ib_search.setOnQueryTextListener(this)
        toolbar_main_activity_ib_sort.setOnClickListener {
            presenter.onSortClicked()
        }
        toolbar_main_activity_tv_title.text = getString(R.string.app_name)
    }

    private fun initCartView() {
        main_activity_rv_cart_items.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        main_activity_rv_cart_items.adapter = CartItemAdapter(mutableListOf(),
                presenter::onItemAdded,
                presenter::onItemRemoved
        )
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList(BEER_ITEMS, getBeerItems())
    }

    override fun onLoadBeerItems(items: List<BeerItem>) {
        main_activity_rv_beer_items.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        main_activity_rv_beer_items.adapter = BeerItemAdapter(items.toMutableList(),
                presenter::onItemAddedToCart)
    }

    override fun showProgressDialog() {
        dialog = MaterialDialog.Builder(this)
                .title(R.string.title)
                .content(R.string.content)
                .show()
    }

    override fun addItemToCart(beerItem: BeerItem) {
        (main_activity_rv_cart_items.adapter as CartItemAdapter).addItem(beerItem)
    }

    override fun hideProgressDialog() {
        dialog.hide()
    }

    override fun getCartItems(): List<CartItem> = (main_activity_rv_cart_items.adapter as CartItemAdapter).getItems()

    override fun getBeerItems(): ArrayList<BeerItem> = (main_activity_rv_beer_items.adapter as BeerItemAdapter).getItems()

    override fun setItemTotal(size: Int) {
        main_activity_tv_total_items.text = String.format(getString(R.string.total_items), size)
    }

    override fun hideBottomView() {
        main_activity_sliding_layout.panelState = SlidingUpPanelLayout.PanelState.HIDDEN
    }

    override fun filteredItems(filter: List<BeerItem>) {
        (main_activity_rv_beer_items.adapter as BeerItemAdapter).refreshWithSearchResults(filter)
    }

    override fun showBottomView() {
        if (main_activity_sliding_layout.panelState != SlidingUpPanelLayout.PanelState.EXPANDED) {
            main_activity_sliding_layout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun onError(msg: String) {
        alert {
            title = "Error"
            message = msg
            positiveButton("ok") {}
        }.show()
    }

    override fun setSortedItems(items: List<BeerItem>) {
        (main_activity_rv_beer_items.adapter as BeerItemAdapter).refreshWithSearchResults(items)
        main_activity_rv_beer_items.layoutManager?.scrollToPosition(0)
    }

    override fun sortedIconDesc() {
        toolbar_main_activity_ib_sort.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_sort_desc_white_24dp))
    }
    override fun sortedIconAsce() {
        toolbar_main_activity_ib_sort.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_sort_asce_white_24dp))
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return true
    }

    override fun onQueryTextChange(newText: String): Boolean {
        presenter.filterItems(newText)
        return true
    }

    companion object {
        const val BEER_ITEMS = "main_activity_beer_items"
    }
}
