package beercraft.chibde.com.beercraft.main.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import beercraft.chibde.com.beercraft.R
import beercraft.chibde.com.beercraft.model.BeerItem
import beercraft.chibde.com.beercraft.model.CartItem
import kotlinx.android.synthetic.main.item_cart.view.*

/**
 * Created by gautam on 30/6/18.
 */
class CartItemAdapter(private var items: MutableList<CartItem>,
                      private val onItemAdded: () -> Unit,
                      private val onItemRemoved: () -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<CartItemAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_cart, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder.itemView) {
            item_cart_tv_name.text = items[position].beerItem.name
            item_cart_tv_quantity.text = items[position].quantity.toString()
            item_cart_ib_add.setOnClickListener {
                items[position].quantity++
                item_cart_tv_quantity.text =items[position].quantity.toString()
                onItemAdded()
            }
            item_cart_ib_remove.setOnClickListener {
                items[position].quantity--
                item_cart_tv_quantity.text =items[position].quantity.toString()
                if(items[position].quantity == 0) {
                    items.removeAt(position)
                    notifyDataSetChanged()
                }
                onItemRemoved()
            }
        }
    }

    override fun getItemCount(): Int = items.size

    class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)

    fun addItem(beerItem: BeerItem) {
        items.firstOrNull {
            it.beerItem == beerItem
        }?.let {
            items[items.indexOf(it)].quantity++
            notifyItemChanged(items.indexOf(it))
        } ?: run {
            items.add(CartItem(beerItem, 1))
            notifyItemInserted(items.size)
        }
    }

    fun getItems(): List<CartItem> = ArrayList(items)
}