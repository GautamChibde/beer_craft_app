package beercraft.chibde.com.beercraft.main

import beercraft.chibde.com.beercraft.ApiClient
import beercraft.chibde.com.beercraft.ApiService
import beercraft.chibde.com.beercraft.main.interactor.SortBeerItemInteractor
import beercraft.chibde.com.beercraft.model.BeerItem
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers

/**
 * Created by gautam on 30/6/18.
 */

class MainActivityPresenter(private val view: MainActivityView) {

    private var apiService: ApiService = ApiClient.retrofit?.create(ApiService::class.java)!!
    private val disposables = CompositeDisposable()
    private var sorted = SORTED_ASCE
    fun onInit() {
        view.showProgressDialog()
        getBeerItems()
    }

    private fun getBeerItems() {
        apiService.getBeerItems()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ items ->
                    run {
                        view.onLoadBeerItems(items)
                        view.hideProgressDialog()
                        updateBottomLayout()
                    }
                }, { error ->
                    run {
                        view.onError(error.message!!)
                    }
                }).addTo(disposables)
    }

    private fun updateBottomLayout() {
        if (view.getCartItems().isEmpty()) {
            view.hideBottomView()
        } else {
            view.showBottomView()
        }
    }

    fun onItemAddedToCart(beerItem: BeerItem) {
        view.addItemToCart(beerItem)
        updateTotal()
    }

    fun onItemAdded() {
        updateTotal()
    }

    private fun updateTotal() {
        var total = 0
        view.getCartItems().forEach {
            total += it.quantity
        }
        view.setItemTotal(total)
        updateBottomLayout()
    }

    fun onItemRemoved() {
        updateTotal()
    }

    fun onSortClicked() {
        SortBeerItemInteractor()
                .execute(sorted, view.getBeerItems())
                .subscribe({ items ->
                    run {
                        view.setSortedItems(items)
                    }
                }).addTo(disposables)

        if (sorted == SORTED_ASCE) {
            sorted = SORTED_DESC
            view.sortedIconDesc()
        } else {
            sorted = SORTED_ASCE
            view.sortedIconAsce()
        }
    }

    fun filterItems(newText: String) {
        Observable.fromIterable(view.getBeerItems())
                .filter { it.name.contains(newText, true) }
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ items ->
                    run {
                        view.filteredItems(items)
                    }
                }, { error ->
                    run {
                        view.onError(error.message!!)
                    }
                }).addTo(disposables)
    }

    fun onDestroy() {
        disposables.dispose()
    }

    companion object {
        const val SORTED_ASCE = 1
        const val SORTED_DESC = 2
    }
}
