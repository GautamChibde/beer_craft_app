package beercraft.chibde.com.beercraft.main.adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import beercraft.chibde.com.beercraft.R
import beercraft.chibde.com.beercraft.model.BeerItem
import kotlinx.android.synthetic.main.item_beer.view.*


/**
 * Created by gautam on 30/6/18.
 */
class BeerItemAdapter(private var items: MutableList<BeerItem>,
                      private val onItemAddedToCart: (BeerItem) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<BeerItemAdapter.ViewHolder>() {

    private val originalList: List<BeerItem> = ArrayList(items)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_beer, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder.itemView) {
            item_beer_tv_name.text = items[position].name
            item_beer_tv_alcohol_content.text = String.format(context.getString(R.string.alcohol_content), items[position].getAlcoholContent().toString())
            item_beer_tv_style.text = items[position].style
            item_beer_ib_add_to_cart.setOnClickListener { onItemAddedToCart(items[position]) }
        }
    }

    override fun getItemCount(): Int = items.size

    class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)

    fun getItems(): ArrayList<BeerItem> = ArrayList(originalList)

    fun refreshWithSearchResults(filteredItems: List<BeerItem>) {
        applyAndAnimateRemovals(filteredItems)
        applyAndAnimateAdditions(filteredItems)
        applyAndAnimateMovedItems(filteredItems)
    }

    private fun applyAndAnimateRemovals(newModels: List<BeerItem>) {
        for (i in items.size - 1 downTo 0) {
            val model = items[i]
            if (!newModels.contains(model)) {
                removeItem(i)
            }
        }
    }

    private fun applyAndAnimateAdditions(newModels: List<BeerItem>) {
        var i = 0
        val count = newModels.size
        while (i < count) {
            val model = newModels[i]
            if (!items.contains(model)) {
                addItem(i, model)
            }
            i++
        }
    }

    private fun applyAndAnimateMovedItems(newModels: List<BeerItem>) {
        for (toPosition in newModels.indices.reversed()) {
            val model = newModels[toPosition]
            val fromPosition = items.indexOf(model)
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition)
            }
        }
    }

    private fun removeItem(position: Int): BeerItem {
        val model = items.removeAt(position)
        notifyItemRemoved(position)
        return model
    }

    private fun addItem(position: Int, model: BeerItem) {
        items.add(position, model)
        notifyItemInserted(position)
    }

    private fun moveItem(fromPosition: Int, toPosition: Int) {
        val model = items.removeAt(fromPosition)
        items.add(toPosition, model)
        notifyItemMoved(fromPosition, toPosition)
    }
}